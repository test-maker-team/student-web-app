const DefaultLayout = ({children}) => {
    return (
        <div className="sm:container mx-auto bg-gray-100">
            <div className="flex content-center bg-white max-h-screen">
                {children}
            </div>
        </div>
    );
}

export default DefaultLayout;
